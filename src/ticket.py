import pickle
import datetime
import logging


# logging.basicConfig(level=logging.DEBUG)


class TicketGrantingService(object):
	def __init__(self, client, ss, K_c_ss,
				period=datetime.timedelta(minutes=3)
				):
		logger = logging.getLogger()
		self.client = client
		self.ss = ss
		self.time = datetime.datetime.now()
		self.period = period
		self.K_c_ss = K_c_ss
		logger.debug('TGS={0} created'.format(self))

	def __repr__(self):
		return str(self.__dict__)

class TicketGrantingTicket(object):
	def __init__(self, client, tgs, K_c_tgs,
				period=datetime.timedelta(minutes=3)
		):
		logger= logging.getLogger()
		self.client = client
		self.tgs = tgs
		self.time = datetime.datetime.now()
		self.period = period
		self.K_c_tgs = K_c_tgs
		logger.debug('TGT={0} created'.format(self))
	
	def __repr__(self):
		return str(self.__dict__)

class Authenticator(object):
	def __init__(self, client_login):
		self.client = client_login
		self.time = datetime.datetime.now()

	def __repr__(self):
		return str(self.__dict__)

		