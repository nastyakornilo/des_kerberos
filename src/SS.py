import cryptoserver
import datetime
import socketserver
import logging
import pickle
import des


SS_HOST, SS_PORT = "localhost", 9997
BUFFER_SIZE = 2048

K_tgs_ss = 'K_tgs_ss'
TIME_DELTA =datetime.timedelta(minutes=2)


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
logger.debug('''
	SS_HOST, SS_PORT: {0}, {1}
	K_tgs_ss: {2}
	TIME_DELTA: {3}
	'''.format(SS_HOST, SS_PORT, K_tgs_ss, TIME_DELTA))



class ServiceServerHandler(socketserver.BaseRequestHandler):
	def is_good_sender(self, TGS, auth):
		if auth.client == TGS.client:
			self.log.debug('OK: auth.client == TGS.client')
			if TGS.time <=auth.time <= TGS.time + TGS.period:
				self.log.debug('OK: TGS.time <=auth.time <= TGS.time + TGS.period')
				return True
			else:
				self.log.debug('Failed: TGS.time <=auth.time <= TGS.time + TGS.period')
		else:
			self.log.debug('Failed: auth.client == TGS.client')
		return False


	def handle(self):
		self.log = logging.getLogger()

		package = self.request.recv(BUFFER_SIZE).strip()
		encrypted_TGS, encrypted_auth = pickle.loads(package)
		self.log.debug('''
			Received packages: encrypted_TGS - {0}, encrypted_auth - {1}
			'''.format(encrypted_TGS.encode(), encrypted_auth.encode()))

		TGS = self.server.decrypt(encrypted_TGS, K_tgs_ss)
		self.log.debug('Decrypted TGS - {0}'.format(TGS))
		
		auth = self.server.decrypt(encrypted_auth, TGS.K_c_ss)
		self.log.debug('Decrypted auth - {0}'.format(auth))

		if self.is_good_sender(TGS, auth):
			time = auth.time + TIME_DELTA
			self.log.debug("Time to sent = auth.time + TIME_DELTA = {0}".format(time))

			encrypted_package = self.server.encrypt(time, TGS.K_c_ss)
			self.request.sendall(encrypted_package.encode())
			self.log.debug(
				"Package sent to client: encrypted (with K_c_ss) time: {0}".format(
					encrypted_package.encode()
					)
				)


if __name__ == "__main__":
	try:
		server = cryptoserver.CryptoServer((SS_HOST, SS_PORT), ServiceServerHandler)
		server.serve_forever()
	except KeyboardInterrupt:
		server.server_close()

