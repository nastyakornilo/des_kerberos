import socketserver
import cryptoserver
import pickle
import logging
import des
from ticket import TicketGrantingService, Authenticator


#class Mandat
TGS_HOST, TGS_PORT = "localhost", 9998
K_as_tgs = 'K_as_tgs'
BUFFER_SIZE = 2048

K_tgs_ss = 'K_tgs_ss'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
# logger.debug('AS_HOST: {0}, AS_PORT: {1}'.format(AS_HOST, AS_PORT))
logger.debug('TGS_HOST: {0}, TGS_PORT: {1}'.format(TGS_HOST, TGS_PORT))
# logger.debug('SS_HOST: {0}, SS_PORT: {1}'.format(SS_HOST, SS_PORT))

# class KerberosServerPart(socketserver.BaseRequestHandler):
# 	def validate_client_id(client_in_ticket, client_in_auth):
# 		if 


class TicketGrantingServerHandler(socketserver.BaseRequestHandler):
	def create_session_key(self):
		return "K_c_ss_1"

	def is_good_sender(self, TGT, auth):
		if auth.client == TGT.client:
			self.log.debug('OK: {0} (auth.client)== {1} TGT.client'.format(auth.client, TGT.client))
			if TGT.time <=auth.time <= (TGT.time + TGT.period):
				self.log.debug('''
					OK: {0}(TGT.time) <= {1} auth.time <= {2} (TGT.time + TGT.period)
					'''.format(TGT.time, auth.time, TGT.time + TGT.period))
				return True
			else:
				self.log.debug('''
					Failed: {0}(TGT.time) <= {1} auth.time <= {2} (TGT.time + TGT.period)
					'''.format(TGT.time, auth.time, TGT.time + TGT.period))
		else:
			self.log.debug('Failed: {0} (auth.client)== {1} TGT.client'.format(auth.client, TGT.client))
		return False

	def handle(self):
		self.log = logging.getLogger()
		package = self.request.recv(BUFFER_SIZE).strip()
		self.log.debug('Package {0} received'.format(package))

		encrypted_TGT, encrypted_auth, service_id = pickle.loads(package)
		self.log.debug('''
			Decrypted package: encrypted_TGT - {0}, encrypted_auth - {1}, service_id - {2}
			'''.format(encrypted_TGT.encode(), encrypted_auth.encode(), service_id))

		TGT = self.server.decrypt(encrypted_TGT, K_as_tgs)
		self.log.debug('Decrypted TGT - {0}'.format(TGT))
		auth = self.server.decrypt(encrypted_auth, TGT.K_c_tgs)
		self.log.debug('Decrypted auth - {0}'.format(auth))

		if self.is_good_sender(TGT, auth):
			K_c_ss = self.create_session_key()
			self.log.debug('Session key created: {0}'.format(K_c_ss))

			TGS = TicketGrantingService(TGT.client, service_id, K_c_ss)
			self.log.debug('TGS - {0}'.format(TGS))

			encrypted_TGS = self.server.encrypt(TGS, K_tgs_ss)
			self.log.debug('Encrypted TGS - {0}'.format(encrypted_TGS.encode()))

			packages = [encrypted_TGS, K_c_ss]
			self.log.debug('''
			Package sent to client (before encryption on K_c_ss): {0}
			(Package parts:  encrypted_TGS, K_c_ss)
			'''.format(packages))


			encrypted_packages = self.server.encrypt(packages, TGT.K_c_tgs)
			self.request.sendall(encrypted_packages.encode())

			
			self.log.debug('''
			Package sent to client encrypted on K_c_ss: {0}
			(Package parts:  encrypted_TGS, K_c_ss)
			'''.format(encrypted_packages.encode()))


if __name__ == "__main__":
	try:
		server = cryptoserver.CryptoServer((TGS_HOST, TGS_PORT), TicketGrantingServerHandler)
		server.serve_forever()
	except KeyboardInterrupt:
		server.server_close()
