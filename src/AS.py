import cryptoserver
import hashlib

# import pickle
import des
import logging
import socketserver
from ticket import TicketGrantingTicket


AS_HOST, AS_PORT = "localhost", 9996
TGS_HOST, TGS_PORT = "localhost", 9998
TGS_ID = (TGS_HOST, TGS_PORT)
BUFFER_SIZE = 2048


CLIENTS = {'nkornilo': hashlib.md5('keykey11'.encode('utf-8')).hexdigest()}

K_as_tgs = 'K_as_tgs'
#client_login - password

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

logger.debug('''\
	AS_HOST, AS_PORT = {0}, {1}
	TGS_HOST, TGS_PORT = {2}, {3}
	TGS_ID = {4}
	CLIENTS = 'nkornilo': hash('keykey11')={5}
	K_as_tgs = {6}
	'''.format(AS_HOST, AS_PORT, TGS_HOST, TGS_PORT, (TGS_HOST, TGS_PORT),CLIENTS['nkornilo'], K_as_tgs))
#

# TODO Find how to pass param to Handler init!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
class AuthenticationServerHandler(socketserver.BaseRequestHandler):
	def handle(self):
		self.log = logging.getLogger()
		login = self.request.recv(BUFFER_SIZE).strip().decode("utf-8")
		if login:
			self.log.debug('Login {0} received'.format(login))
			
			K_c = self.get_client_secret_key(login)

			if K_c is not None:
				K_c_tgs = self.create_session_key()
				TGT = TicketGrantingTicket(login, TGS_ID, K_c_tgs)    #ticket for access to tgs
				# self.log.debug('TGT={0} created'.format(TGT))
				TGT_encrypted = self.server.encrypt(TGT,K_as_tgs)
				# self.log.debug('Ecrypted TGT: {0}'.format(TGT_encrypted))
				
				package = [TGT_encrypted, K_c_tgs]
				self.log.debug('''
					Package sent to client (before encryption on K_c):
					encrypted_TGT : {0} 
					K_c_tgs : {1}
					'''.format(package[0].encode(), package[1]))

				encrypted_msg = self.server.encrypt(package, K_c).encode()

				self.request.sendall(encrypted_msg)
				self.log.debug('''
					Package sent to client - (encrypted TGT, K_c_tgs)K_c: {0} 
					'''.format(encrypted_msg))

	def get_client_secret_key(self, login):
		is_exists = login in CLIENTS.keys()
		if is_exists:
			K_c = CLIENTS[login]
			self.log.debug('User {0} exists. Secret key {1}'.format(login, K_c))
			return K_c
		else:
			self.log.debug('User {0} does not exist'.format(login))

	def create_session_key(self):
		K_c_tgs = 'K_c_tgs1'
		self.log.debug('Session key K_c_tgs={0} created'.format(K_c_tgs))
		return K_c_tgs
        

if __name__ == "__main__":
    try:
        server = cryptoserver.CryptoServer((AS_HOST, AS_PORT), AuthenticationServerHandler)
        server.serve_forever()
    except KeyboardInterrupt:
    	server.server_close()



