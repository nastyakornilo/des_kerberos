import socketserver
import des
import logging
import codecs
import pickle


class CryptoServer(socketserver.ThreadingTCPServer):

	def __init__(self, server_address, RequestHandlerClass, cryptographer= des.DES()):
		socketserver.ThreadingTCPServer.__init__(self, 
												server_address, 
												RequestHandlerClass)
		self.cryptographer = cryptographer
		self.log = logging.getLogger()
		self.log.debug("Serving on {}".format(server_address))

	def encrypt(self, obj, key):
		str_obj = codecs.encode(pickle.dumps(obj), "base64").decode()

		encrypted_msg = self.cryptographer.encrypt(
							str_obj,
							key
						)
		return encrypted_msg

	def decrypt(self, message, key):
		decrypted_msg = self.cryptographer.decrypt(
							message,
							key

						)
		try:
			obj= pickle.loads(codecs.decode(decrypted_msg.encode(), "base64"))
			return obj
		except:
			self.log.error("Wrong decryption key was used")
			raise Exception("Wrong decryption key was used")

	def serve_forever(self):
		super().serve_forever()
		self.log.debug("Start serving on {}".format(self.server_address))
