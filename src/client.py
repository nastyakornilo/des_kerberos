import pickle
import time
import hashlib
import codecs
import socket
import sys
import datetime
import logging
import des
from ticket import Authenticator


AS_HOST, AS_PORT = "localhost", 9996
TGS_HOST,TGS_PORT = "localhost", 9998
SS_HOST, SS_PORT = "localhost", 9997

BUFFER_SIZE = 2048
TIME_DELTA =datetime.timedelta(minutes=2)


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


logger.debug('AS_HOST: {0}, AS_PORT: {1}'.format(AS_HOST, AS_PORT))
logger.debug('TGS_HOST: {0}, TGS_PORT: {1}'.format(TGS_HOST, TGS_PORT))
logger.debug('SS_HOST: {0}, SS_PORT: {1}'.format(SS_HOST, SS_PORT))




class SocketClient(object):
	def __init__(self):
		self.socket = None

	def connect(self, host=AS_HOST, port=AS_PORT):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((host, port))

	def _send(self, data):
		print (self.socket)
		if isinstance(data, bytes):
			self.socket.sendall(data)
		else:
			self.socket.sendall(data.encode())

	def send(self, data, host=AS_HOST, port=AS_PORT):
		self.connect(host, port)
		self._send(data)

	def receive(self, size=BUFFER_SIZE):
		received = self.socket.recv(size).strip().decode()
		self.disconnect()
		return received

	def disconnect(self):
		if self.socket is not None:
			self.socket.close()
		self.socket = None


class KerberosClient(SocketClient):
	def __init__(self, login, password, cryptographer=des.DES()):
		self.log = logging.getLogger()
		self.login = login
		self.K_c = hashlib.md5(password.encode('utf-8')).hexdigest()
		self.cryptographer = cryptographer

		self.log.debug("Client login:{0}, password:{1}, secret_key:{2}".format(
			self.login, password, self.K_c
			)
		)

	def authenticate(self, as_address=(AS_HOST, AS_PORT)):
		self.send(self.login, *as_address)
		self.log.debug('Authentication. Login: {0}, AS address: {1}'.format(self.login, as_address))

	def receive_AS_reply(self):
		package = self.receive()
		if package=='':
			raise Exception('User does not exists')
		self.log.debug('Received from AS: {0}'.format(package.encode()))

		try:
			_, self.K_c_tgs  = self.decrypt(package, self.K_c)  #K_c_tgs-session_key
		except:
			self.log.error("Wrong password")
			raise Exception("Wrong password")

		self.encrypted_TGT = _

		self.log.debug('''
			After decryption package with K_c (secret key): 
			encrypted_TGT: {0}, 
			K_c_tgs: {1}'
			'''.format(self.encrypted_TGT.encode(), self.K_c_tgs)
			)
		

	def service_authorizate(self, tgs_address=(TGS_HOST, TGS_PORT), service_id=(SS_HOST, SS_PORT)):              #in more general case here ID
		self.log.debug('''
			Authorization. TGS_adress: {0}'. Service_id: {1}
			'''.format(tgs_address, service_id)
			)

		self.auth = Authenticator(self.login)
		self.log.debug('Authenticator {0} created'.format(self.auth))

		encrypted_auth = self.encrypt(self.auth, self.K_c_tgs)
		self.log.debug('Authenticator encrypted with K_c_tgs'.format(encrypted_auth.encode()))

		packages = [self.encrypted_TGT, encrypted_auth, service_id]
		packages_dump = pickle.dumps(packages)

		self.send(packages_dump, *tgs_address)
		self.log.debug('''
			Sent to TGS:
			ecrypted_TGT: {0}, ecrypted_auth: {1}, service_id: {2}
			'''.format(packages[0].encode(), packages[1].encode(), packages[2])
			)


	def receive_TGS_reply(self):
		#TODO: add TGS to dict
		package = self.receive()
		if package=='':
			raise Exception('Access to TGS failed')     #TODO in TGS good check for time and client_id validation

		self.log.debug('Received from TGS: {0}'.format(package.encode()))

		self.encrypted_TGS, self.K_c_ss = self.decrypt(package, self.K_c_tgs)
		self.log.debug('''
			After decryption package with K_c_tgs: 
			enctypted_TGS: {0}, 
			K_c_ss: {1}
			'''.format(self.encrypted_TGS.encode(), self.K_c_ss)
			)

	def service_request(self, ss_address=(SS_HOST, SS_PORT)):				#in more general case here ID
		self.auth = Authenticator(self.login)
		self.log.debug('Authenticator {0} created'.format(self.auth))

		encrypted_auth = self.encrypt(self.auth, self.K_c_ss)
		self.log.debug('Authenticator encrypted with K_c_ss'.format(encrypted_auth.encode()))

		packages = [self.encrypted_TGS, encrypted_auth]
		package = pickle.dumps(packages)
		# ss_address = self.service_servers['service_id']						#in more general case here ID
		self.send(package, *ss_address)
		self.log.debug('''
			Packages for send to SS:
			ecrypted_TGS: {0}, 
			ecrypted_auth: {1}
			'''.format(packages[0].encode(), packages[1].encode())
			)

	def verify_ss(self):
		package = self.receive()
		self.log.debug('Received from SS: {0}'.format(package.encode()))

		expected_time = self.auth.time + TIME_DELTA
		received_time = self.decrypt(package, self.K_c_ss)

		self.log.debug('''
			Expected time: {0}. Received (got after encrypt):{1}
			'''.format(expected_time, received_time)
			)
		if expected_time == received_time:
			self.log.debug('Successful checking')
		else:
			self.log.debug('Checking failed')
		return expected_time == received_time

	def encrypt(self, obj, key):
		str_obj = codecs.encode(pickle.dumps(obj), "base64").decode()

		encrypted_msg = self.cryptographer.encrypt(
							str_obj,
							key
						)
		return encrypted_msg

	def decrypt(self, message, key):
		decrypted_msg = self.cryptographer.decrypt(
							message,
							key

						)
		try:
			obj= pickle.loads(codecs.decode(decrypted_msg.encode(), "base64"))
			return obj
		except:
			self.log.error("Wrong decryption key was used")
			raise Exception("Wrong decryption key was used")
		



login = input('Login: ')
password = input('Password: ')
#login='nkornilo'
#password = 'keykey11'
client = KerberosClient(login, password)

# while True:
try:
	# Connect to server and send data
	client.authenticate()
	client.receive_AS_reply()
	print('Successful authentication')
	time.sleep(2)

	client.service_authorizate()
	client.receive_TGS_reply()
	print('Successful authorization')

	client.service_request()
	if client.verify_ss():
		print ("Got access to required service. Exit")
	else:
		print ("Attempt get access to required service was failed . Try again")

except Exception as e:
	print (e)

	# Receive data from the server and shut down
finally:
	client.disconnect()
		



