# initial permutation IP
ip = [ 57, 49, 41, 33, 25, 17, 9,  1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7,
    56, 48, 40, 32, 24, 16, 8,  0,
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6
]

# final permutation IP^-1
fp = [39,  7, 47, 15, 55, 23, 63, 31,
    38,  6, 46, 14, 54, 22, 62, 30,
    37,  5, 45, 13, 53, 21, 61, 29,
    36,  4, 44, 12, 52, 20, 60, 28,
    35,  3, 43, 11, 51, 19, 59, 27,
    34,  2, 42, 10, 50, 18, 58, 26,
    33,  1, 41,  9, 49, 17, 57, 25,
    32,  0, 40,  8, 48, 16, 56, 24
]

expansion_table = [
    31,  0,  1,  2,  3,  4,
    3,  4,  5,  6,  7,  8,
    7,  8,  9, 10, 11, 12,
    11, 12, 13, 14, 15, 16,
    15, 16, 17, 18, 19, 20,
    19, 20, 21, 22, 23, 24,
    23, 24, 25, 26, 27, 28,
    27, 28, 29, 30, 31,  0
]

sbox = [
[
    [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
    [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
    [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
    [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
],

[
    [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
    [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
    [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
    [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9],
],

[
    [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
    [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
    [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
    [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12],
],

[
    [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
    [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
    [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
    [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14],
],  

[
    [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
    [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
    [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
    [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3],
], 

[
    [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
    [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
    [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
    [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13],
], 

[
    [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
    [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
    [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
    [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12],
],
   
[
    [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
    [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
    [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
    [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11],
]
]


#Permutation made after each SBox substitution for each round
p = [15, 6, 19, 20, 28, 11, 27, 16,
     0, 14, 22, 25, 4, 17, 30, 9,
     1, 7, 23, 13, 31, 26, 2, 8,
     18, 12, 29, 5, 21, 10, 3, 24
]

pc1 = [
    56, 48, 40, 32, 24, 16,  8, 0, 
    57, 49, 41, 33, 25, 17, 9,  1, 
    58, 50, 42, 34, 26, 18, 10, 2, 
    59, 51, 43, 35, 
    62, 54, 46, 38, 30, 22, 14, 6, 
    61, 53, 45, 37, 29, 21, 13, 5, 
    60, 52, 44, 36, 28, 20, 12, 4, 
    27, 19, 11,  3
]

pc2 = [
        13, 16, 10, 23,  0,  4,
         2, 27, 14,  5, 20,  9,
        22, 18, 11,  3, 25,  7,
        15,  6, 26, 19, 12,  1,
        40, 51, 30, 36, 46, 54,
        29, 39, 50, 44, 32, 47,
        43, 48, 38, 55, 33, 52,
        45, 41, 49, 35, 28, 31
]

#Determine the shift for each round of keys
left_shifts = [
    1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
]

def nsplit(l, n):#Split a list into sublists of size "n"
    return [l[k:k+n] for k in range(0, len(l), n)]

def xor_bit_arrays(arr1, arr2):# xor bit_arrays
    return [x^y for x,y in zip(arr1,arr2)]

def shift_list(arr, n):     #positive n for left shift, negative - for right
    return arr[n:] + arr[:n]

def tobits(s):
    result = []
    for c in s:
        bits = bin(ord(c))[2:]
        bits = '00000000'[len(bits):] + bits
        result.extend([int(b) for b in bits])
    return result

def frombits(bits):   #string from array of bits

    chars = []
    _bytes = nsplit(bits, 8)
    for byte in _bytes:
        byte_str = ''.join([str(bit) for bit in byte])
        # print(byte_str)
        # print(chr(int(byte_str, 2)))
        chars.append(chr(int(byte_str, 2)))

    return ''.join(chars)


# def string_to_bit_array(text):#Convert a string into a list of bits
#     text.encode()
#     array = list()
#     for char in text:
#         binval = binvalue(char, 8)#Get the char value on one byte
#         array.extend([int(x) for x in list(binval)]) #Add the bits to the final list
#     return array

# def bit_array_to_string(array): #Recreate the string from the bit array
#     res = ''.join([chr(int(y,2)) for y in [''.join([str(x) for x in bytes]) for bytes in  nsplit(array,8)]])   
#     return res


ENCRYPT = 0
DECRYPT = 1


class DES(object):
    def __init__(self):   #add strip
        # self.key_len = 56
        # if len(key) < self.key_len:
        #     raise ValueError("Key should be 56 bytes long")
        # if not isinstance(key, bytes):
        #   raise TypeError("Key should be bytes list")
        self.key = None
        self.subkeys = None
        self.block_size = 64


    def run(self, data, key, do_pad=False, action=ENCRYPT):
        if not data:
            return ''

        if len(key) < 64:
            raise ValueError("Invalid key length. Key length must be >=64")
        key  = key[:63]

        self.key = key

        self.create_subkeys()

        if do_pad:
            data = self._pad_data(data)

        if len(data) % self.block_size != 0:
                raise ValueError("Invalid text length")
        

        # if len(data) % self.block_size != 0:
        #     if action == DECRYPT:
        #         raise ValueError("Invalid text length")
        #     elif not do_pad:
        #         raise ValueError("Invalid text length. Try option do_pad")
        #     else:
        #         self._pad_data(data)
        
        # data = string_to_bit_list

        blocks = nsplit(data, self.block_size)
        result = []
        for block in blocks:
            result.extend(self.crypt_block(block, action))
        print(len(result))
        return result

    def permutate(self, table, block):
        return [block[elem]for elem in table]

    def crypt_block(self, block, action=ENCRYPT):
        block = self.permutate(ip, block)
        L, R = nsplit(block, int(self.block_size/2))
        
        if action == ENCRYPT:
            key_number = 0
            step = 1
        else:
            key_number = 15
            step = -1
        for i in range(16):
            R_copy = R[:]
            subkey = self.subkeys[key_number]
            E = self.permutate(expansion_table, R)  # expand R

            B = xor_bit_arrays(E, subkey)
            B = self.apply_sboxes(B)

            F = self.permutate(p, B)
            R = xor_bit_arrays(L, F)
            L = R_copy
            key_number += step
        return self.permutate(fp, R+L)

    def apply_sboxes(self, R_block): # 48-bit arr -> 32-bit arr
        subblocks = nsplit(R_block, 6)
        result = []
        for i, subblock in enumerate(subblocks):
            base = 2
            #get row with first and last bit by concating first and last bit and convert to int
            row = int(str(subblock[0])+str(subblock[5]), base)
            #get rcolumn with 2-5 bits by concating 2-5 bits and convert to int
            col = int(''.join([str(x) for x in subblock[1:-1]]), base)

            
            val = sbox[i][row][col]
            bin_str = format(val, '04b')  # value as binary string with leading zeros
            result += [int(x) for x in bin_str]
        return result


    def create_subkeys(self):
        self.subkeys = []        
        self.key = self.permutate(pc1, self.key) #initial permutation on the key
        
        C, D = nsplit(self.key, int(56/2)) #Split on LEFT and RIGHT
        for i in range(16):   #16 rounds
            C = shift_list(C, left_shifts[i])
            D = shift_list(D, left_shifts[i])
            # print('C{0}:{1} \nD{0}:{2}'.format(i,C,D))
            tmp = C+D
            self.subkeys.append(self.permutate(pc2, tmp)) #Apply the permutation to get the Ki

    def _pad_data(self, data):
        # Pad data depending on the mode
        
        # data += (self.block_size - (len(data) % self.block_size)) * pad
        pad_len = self.block_size - (len(data) % self.block_size)
        # append 100..0
        return data+[1]+([0]*(pad_len-1))

    def _unpad_data(self, data):
        #data has structure: our_info 1 0 0 ... 0
        trash_zeros_count = data[::-1].index(1)   #get trash_zeros_count by finding first from end value=1
        trash_count = trash_zeros_count + 1
        return data[:-trash_count]

    def encrypt(self, data, key, do_pad=True):
        # if isinstance(data, bytes):  #k
        #     data = data.decode()
        if isinstance(data, str):
            data = tobits(data)
        if isinstance(key, int):
            key=bin(key)[2:]
        if isinstance(key, str):
            key = tobits(key)
        return frombits(self.run(data, key, do_pad, action=ENCRYPT))

    def decrypt(self, data, key, do_unpad=True):
        # if isinstance(data, bytes):  #k
        #     data = data.decode()
        if isinstance(data, str):
            data = tobits(data)
        if isinstance(key, int):
            key=bin(key)[2:]

        if isinstance(key, str):
            key = tobits(key)
        
        without_unpad = self.run(data,key, action=DECRYPT)

        if do_unpad:
            return frombits(self._unpad_data(without_unpad))
        return frombits(without_unpad)


def example():
    k = [0,0,0,1,0,0,1,1, 0,0,1,1,0,1,0,0, 0,1,0,1,0,1,1,1, 0,1,1,1,1,0,0,1, 1,0,0,1,1,0,1,1, 1,0,1,1,1,1,0,0, 1,1,0,1,1,1,1,1, 1,1,1,1,0,0,0,1]
    m = [0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1, 0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1, 1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1, 1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1]
    d = DES()
    e = d.encrypt(m, k)
    print('encoded', e)
    # print('encoded',frombits(e).encode())
    de = d.decrypt(e, k)
    print ('decoded', de)
    if de == m:
        print('OK')
    else:
        print('Bad')
    # print ('decoded', de)
    # print('decoded',frombits(de).encode())


if __name__ == '__main__':
    example()
    # print('HI')
    # k = tobits('keykey11')
    # inp = 'nkornilo'
    # s = tobits(inp)

    # print ('input', inp)
    # # print('input in bytes:', inp.encode())

    # d = DES(k)
    # e = d.encrypt(s)
    # # print('encoded', e)
    # # print('encoded',frombits(e).encode())
    # de = d.decrypt(e)
    # # print ('decoded', de)
    # # print('decoded',frombits(de).encode())

